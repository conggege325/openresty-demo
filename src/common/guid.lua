math.randomseed(tostring(ngx.now()*1000):reverse())

local function guid()
  
    local tb={}
    
    for i=1,36 do
      if i == 9 or i == 14 or i == 19 or i == 24 then
        table.insert(tb, '-')
      else
        local num = math.random(1, 62)
        local char = nil
        if num <= 10 then
          char = string.char(48 + num -1)
        elseif num <= 36 then
          char = string.char(65 + num - 10 - 1)
        else
          char = string.char(97 + num - 36 - 1)
        end
        table.insert(tb, char)
      end
    end
    
    return table.concat(tb)
end

return guid
