
local mysql = require("resty.mysql")

local mysqldb = {}

function mysqldb.connect()

  local db, e = mysql:new()
  if not db then
    return
  end

  db:set_timeout(10000)

  local props = {
    host = "127.0.0.1",
    port = 3306,
    database = "test",
    user = "root",
    password = "root"
  }

  local res, err, errno, sqlstate = db:connect(props)

  if not res then
    return
  end

  return db
end

function mysqldb.query()
  
end

function mysqldb.close(db)
  if db then
    db:close()
  end
end

return mysqldb

-- http://ju.outofmemory.cn/entry/133272
-- https://blog.csdn.net/forezp/article/details/78616698