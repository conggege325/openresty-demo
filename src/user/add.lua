local cjson = require "cjson"
local mysqldb = require "common.mysqldb"

return function ()
  ngx.req.read_body()
  local args = ngx.req.get_post_args()

  local sql = "insert into users (`login_name`, `nickname`, `login_pwd`, `remark`) values ('"..args["login_name"].."', '"..args["nickname"].."', '123456', '"..args["remark"].."')"
  ngx.log(ngx.ERR, sql)

  local connection = mysqldb.connect()

  local res, err, errno, sqlstate = connection:query(sql)
  if res then
    local obj = {
      ret = 0,
      data = res.insert_id
    }
    ngx.say(cjson.encode(obj))
  end

  mysqldb.close(connection)
end
