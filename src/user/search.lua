local cjson = require "cjson"
local mysqldb = require "common.mysqldb"

return function ()
  local args = ngx.req.get_uri_args()

  local sql = "select `id`, `login_name`, `nickname`, `status`, `reg_time`, `remark` from users"
  if args["username"] and args["username"] ~= "" then
    sql = sql.." where login_name like concat('%', '"..args["username"].."', '%')"
  end
  ngx.log(ngx.ERR, sql)

  local list = {}
  local connection = mysqldb.connect()
  local line
  
  local res, err, errno, sqlstate = connection:query(sql)
  if res then
    for i, row in ipairs(res) do
      line = {}
      for name, value in pairs(row) do
        line[name] = value
      end
      table.insert(list, line)
      line = nil
    end
  end
  
  mysqldb.close(connection)

  local result = string.gsub(cjson.encode(list), "\\/", "/")
  ngx.say(result)
end
