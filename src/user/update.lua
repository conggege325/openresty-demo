local cjson = require "cjson"
local mysqldb = require "common.mysqldb"

return function ()
  ngx.req.read_body()
  local args = ngx.req.get_post_args()

  local sql = "update users set nickname = '"..args["nickname"].."', remark = '"..args["remark"].."' where id = "..args["id"]
  ngx.log(ngx.ERR, sql)

  local connection = mysqldb.connect()

  local res, err, errno, sqlstate = connection:query(sql)
  if res then
    local obj = {
      ret = 0,
      data = res.affected_rows
    }
    ngx.say(cjson.encode(obj))
  end

  mysqldb.close(connection)
end
