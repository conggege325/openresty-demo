local cjson = require "cjson"
local mysqldb = require "common.mysqldb"

return function ()
  ngx.req.read_body()
  local args = ngx.req.get_post_args()

  local sql = "select `login_name`, `login_pwd` from users where login_name = '"..args["login_name"].."'"
  ngx.log(ngx.ERR, sql)

  local connection = mysqldb.connect()
  local logined = false
  
  local res, err, errno, sqlstate = connection:query(sql)
  if res then
    for i, row in ipairs(res) do
      if row.login_pwd == args.login_pwd then
        logined = true
        break
      end
    end
  end
  
  mysqldb.close(connection)

  local obj = {
    ret = 0,
    data = logined
  }
  ngx.say(cjson.encode(obj))
end
