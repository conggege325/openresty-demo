-- mysql
DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login_name` varchar(50) NOT NULL COMMENT '登录账户',
  `login_pwd` varchar(50) NOT NULL COMMENT '录登密码',
  `nickname` varchar(50) DEFAULT '' COMMENT '昵称',
  `status` int(1) DEFAULT '0' COMMENT '状态',
  `reg_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `remark` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login_name` (`login_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` VALUES (1, 'admin', '123456', 'sa', 0, '2018-10-11 10:27:59', NULL);
INSERT INTO `users` VALUES (2, 'Tom', '000011', '汤姆', 0, '2014-12-30 00:55:49', '这个用户是专业差评');
INSERT INTO `users` VALUES (3, 'Jerry', '000000', '杰瑞', 1, '2014-12-30 00:00:00', '6666');
