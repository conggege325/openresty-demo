local arr = ngx.re.match(ngx.var.uri, "^/lua/([-_a-zA-Z0-9]+)(/([-_a-zA-Z0-9]+))?$")

if(arr) then
  local lvl2path = arr[3] and arr[3] or "index"
  local path = arr[1].."."..lvl2path
  local module = require(path)
  
  if(type(module) == "function") then
    module()
    return
  end
end

ngx.exit(404)
