
@ECHO OFF

echo nginx service is being shut down ......

D:

cd D:\dev\openresty-1.13.6.2-win64

nginx.exe -c D:\dev\projects-new\openresty-demo\conf\nginx.conf -s stop

echo nginx service is being started ......

ping localhost -n 5 > nul

start /b nginx.exe -c D:\dev\projects-new\openresty-demo\conf\nginx.conf

echo nginx service has been started successfully !

ping localhost -n 2 > nul
