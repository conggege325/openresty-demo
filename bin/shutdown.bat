
@ECHO OFF

echo nginx service is being shut down ......

D:

cd D:\dev\openresty-1.13.6.2-win64

nginx.exe -c D:\dev\projects-new\openresty-demo\conf\nginx.conf -s stop

ping localhost -n 1 > nul

echo nginx service has been shut down successfully !

ping localhost -n 3 > nul
